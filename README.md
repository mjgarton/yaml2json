yaml2json
=========

An extremely simple YAML to JSON converter, written in Go.

Installation
------------
```
go get gitlab.com/mjgarton/yaml2json
```

