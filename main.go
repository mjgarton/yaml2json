package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

func main() {
	if err := run(); err != nil {
		log.Fatalf("%v\n", err)
	}
}

func run() error {
	var t map[string]interface{}

	dec := yaml.NewDecoder(os.Stdin)
	dec.SetStrict(true)
	if err := dec.Decode(&t); err != nil {
		return fmt.Errorf("error unmarshaling yaml: %v", err)
	}

	if err := json.NewEncoder(os.Stdout).Encode(t); err != nil {
		return fmt.Errorf("error marshalling json: %v", err)
	}

	return nil
}
